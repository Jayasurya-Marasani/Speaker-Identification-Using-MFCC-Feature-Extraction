# Speaker-Identification-Using-MFCC-Feature-Extraction
This GitHub repository contains an implementation of Speaker Identification using MFCC feature extraction. The project includes a comprehensive comparison between GMM, CNN, Random Forest, RNN and KNN for speaker identification tasks.
